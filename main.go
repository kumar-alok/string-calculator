// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
	"regexp"
	"strconv"
	"unicode/utf8"
)

func main() {
	input := "1\n2,3"
	fmt.Println("Result :: ", Add(input))
}

func Add(inp string) int {
	sum := 0
	if inp == "" {
		return sum
	}
	val, _ := strconv.Atoi(inp)
	if len(inp) == 1 {
		return val
	}
	if val < 0 {
		panic("negatives not allowed")
	}
	re, err := regexp.Compile(`[^\w]`)
	if err != nil {
		panic(err)
	}
	inp = re.ReplaceAllString(inp, "")
	for _, val := range inp {
		buf := make([]byte, 1)
		_ = utf8.EncodeRune(buf, val)
		value, _ := strconv.Atoi(string(buf))
		if value < 0 {
			panic("negatives not allowed")
		}
		sum = sum + value
	}
	return sum
}
